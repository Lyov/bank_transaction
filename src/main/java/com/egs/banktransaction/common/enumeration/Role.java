package com.egs.banktransaction.common.enumeration;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}
