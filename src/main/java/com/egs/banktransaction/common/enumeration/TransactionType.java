package com.egs.banktransaction.common.enumeration;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}
