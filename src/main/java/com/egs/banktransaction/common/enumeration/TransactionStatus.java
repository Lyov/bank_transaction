package com.egs.banktransaction.common.enumeration;

public enum TransactionStatus {
    PENDING, ACCEPTED, REJECTED
}
