package com.egs.banktransaction.repository;

import com.egs.banktransaction.entity.UserKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserKeyRepository extends JpaRepository<UserKey, Long> {

    UserKey findByUserId(Long userId);

    UserKey save(UserKey userKey);

}
