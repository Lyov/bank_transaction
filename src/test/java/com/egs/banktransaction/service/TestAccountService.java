package com.egs.banktransaction.service;

import com.egs.banktransaction.common.enumeration.Role;
import com.egs.banktransaction.entity.Account;
import com.egs.banktransaction.entity.User;
import com.egs.banktransaction.entity.UserKey;
import com.egs.banktransaction.repository.AccountRepository;
import com.egs.banktransaction.utils.RandomGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TestAccountService {

    @InjectMocks
    AccountService accountService;

    @Mock
    AccountRepository accountRepository;

    private Account account;

    @BeforeEach
    void setUp() {
        User user = new User();
        user.setId(RandomGenerator.generateRandomLong());
        user.setUuid(RandomGenerator.generateUUID());
        user.setUsername(RandomGenerator.generateRandomString(6));
        user.setPassword( RandomGenerator.generateRandomString(16));
        user.setEmail( RandomGenerator.generateRandomString(6));

        UserKey userKey = new UserKey();
        userKey.setId(RandomGenerator.generateRandomLong());
        userKey.setRole(Role.ROLE_ADMIN);
        userKey.setTokenType(RandomGenerator.generateRandomString(5));
        userKey.setToken(RandomGenerator.generateRandomString(50));
        userKey.setRefreshToken(RandomGenerator.generateRandomString(50));
        userKey.setUserId(RandomGenerator.generateRandomLong());

        account = new Account();
        account.setId(RandomGenerator.generateRandomLong());
        account.setBalance(BigDecimal.valueOf(RandomGenerator.generateRandomLong()));
        account.setCurrency(RandomGenerator.generateRandomString(3));
        account.setUser(user);
    }

    @AfterEach
    public void tearDown() {
        account = null;
    }

    @Test
    void generateAccountTest() {
        Mockito.when(accountRepository.save(any())).thenReturn(account);
        accountService.generateAccount(account);
        verify(accountRepository, times(1)).save(account);
    }
}
