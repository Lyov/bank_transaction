package com.egs.banktransaction.service;

import com.egs.banktransaction.common.enumeration.Role;
import com.egs.banktransaction.entity.User;
import com.egs.banktransaction.entity.UserKey;
import com.egs.banktransaction.repository.UserKeyRepository;
import com.egs.banktransaction.repository.UserRepository;
import com.egs.banktransaction.utils.RandomGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TestUserService {

    @InjectMocks
    UserService userService;

    @Mock
    UserRepository userRepository;

    @Mock
    UserKeyRepository userKeyRepository;

    private User user;

    private UserKey userKey;

    private Long id;

    private String uuid;

    @BeforeEach
     void setUp() {
        id = RandomGenerator.generateRandomLong();
        uuid = RandomGenerator.generateUUID();
        String username = RandomGenerator.generateRandomString(6);
        String password = RandomGenerator.generateRandomString(16);
        String email = RandomGenerator.generateRandomString(6);
        user = new User();
        user.setId(id);
        user.setUuid(uuid);
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        userKey = new UserKey();
        userKey.setId(id);
        userKey.setRole(Role.ROLE_ADMIN);
        userKey.setTokenType(RandomGenerator.generateRandomString(5));
        userKey.setToken(RandomGenerator.generateRandomString(50));
        userKey.setRefreshToken(RandomGenerator.generateRandomString(50));
        userKey.setUserId(id);
    }

    @Test
    void getUserByEmailTest() {
        Mockito.when(userRepository.findByEmail(any())).thenReturn(user);
        assertThat(userService.getUserByEmail(user.getEmail())).isEqualTo(user);
    }

    @Test
    void generateUserTest() {
        Mockito.when(userRepository.save(any())).thenReturn(user);
        userService.generateUser(user);
        verify(userRepository, times(1)).save(user);
    }

    @Test
    void  generateUserKeyTest() {
        Mockito.when(userKeyRepository.save(any())).thenReturn(userKey);
        userService.generateUserKey(userKey);
        verify(userKeyRepository, times(1)).save(userKey);
    }

    @Test
     void getUserByIdAndUUIDTest() {
        Mockito.when(userRepository.findByIdAndUuid(any(), any())).thenReturn(user);
        assertThat(userService.getUserByIdAndUserUuid(user.getId(), user.getUuid())).isEqualTo(user);
    }

    @Test
     void updateUserRoleTest() {
        Mockito.when(userKeyRepository.save(any())).thenReturn(userKey);
        userService.updateUserRole(userKey);
        verify(userKeyRepository, times(1)).save(userKey);
    }



}
