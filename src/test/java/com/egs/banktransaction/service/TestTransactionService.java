package com.egs.banktransaction.service;

import com.egs.banktransaction.common.enumeration.Role;
import com.egs.banktransaction.common.enumeration.TransactionStatus;
import com.egs.banktransaction.common.enumeration.TransactionType;
import com.egs.banktransaction.entity.Account;
import com.egs.banktransaction.entity.Transaction;
import com.egs.banktransaction.entity.User;
import com.egs.banktransaction.entity.UserKey;
import com.egs.banktransaction.repository.TransactionRepository;
import com.egs.banktransaction.utils.RandomGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


@ExtendWith(MockitoExtension.class)
class TestTransactionService {

    @InjectMocks
    TransactionService transactionService;

    @Mock
    TransactionRepository transactionRepository;

    private List<Transaction> transactions;

    private Long id;

    private Date date;

    @BeforeEach
    void setUp() {
        id = RandomGenerator.generateRandomLong();
        date = new Date();
        transactions = new ArrayList<>();

        User user = new User();
        user.setId(RandomGenerator.generateRandomLong());
        user.setUuid(RandomGenerator.generateUUID());
        user.setUsername(RandomGenerator.generateRandomString(6));
        user.setPassword( RandomGenerator.generateRandomString(16));
        user.setEmail( RandomGenerator.generateRandomString(6));

        UserKey userKey = new UserKey();
        userKey.setId(RandomGenerator.generateRandomLong());
        userKey.setRole(Role.ROLE_ADMIN);
        userKey.setTokenType(RandomGenerator.generateRandomString(5));
        userKey.setToken(RandomGenerator.generateRandomString(50));
        userKey.setRefreshToken(RandomGenerator.generateRandomString(50));
        userKey.setUserId(RandomGenerator.generateRandomLong());

        Account account = new Account();
        account.setId(RandomGenerator.generateRandomLong());
        account.setBalance(BigDecimal.valueOf(RandomGenerator.generateRandomInteger()));
        account.setCurrency(RandomGenerator.generateRandomString(3));
        account.setUser(user);

        Transaction transaction = new Transaction();
        transaction.setId(RandomGenerator.generateRandomLong());
        transaction.setStatus(TransactionStatus.PENDING);
        transaction.setAmount(BigDecimal.valueOf(RandomGenerator.generateRandomInteger()));
        transaction.setType(TransactionType.DEPOSIT);
        transaction.setAccount(account);
        transactions.add(transaction);
    }

    @AfterEach
    public void tearDown() {
        transactions = null;
        date = null;
        id = null;
    }

    @Test
    void getUserTransactionsTest() {
        Mockito.when(transactionRepository.findTransactionsByUserId(id)).thenReturn(transactions);
        assertThat(transactionRepository.findTransactionsByUserId(id)).isEqualTo(transactions);

    }

    @Test
    void getUserTransactionsByDateTest() {
        Mockito.when(transactionRepository.findTransactionsByUserIdAndByDate(id, date)).thenReturn(transactions);
        assertThat(transactionRepository.findTransactionsByUserIdAndByDate(id, date)).isEqualTo(transactions);
    }
}
