package com.egs.banktransaction.repository;

import com.egs.banktransaction.common.enumeration.TransactionStatus;
import com.egs.banktransaction.common.enumeration.TransactionType;
import com.egs.banktransaction.entity.Account;
import com.egs.banktransaction.entity.Transaction;
import com.egs.banktransaction.entity.User;
import com.egs.banktransaction.utils.RandomGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TestTransactionRepository {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private User user;

    private Account account;

    private Transaction transaction;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setUuid(RandomGenerator.generateUUID());
        user.setUsername(RandomGenerator.generateRandomString(6));
        user.setPassword( RandomGenerator.generateRandomString(16));
        user.setEmail( RandomGenerator.generateRandomString(4).concat("@gmail.com"));

        account = new Account();
        account.setBalance(BigDecimal.valueOf(RandomGenerator.generateRandomInteger()));
        account.setCurrency(RandomGenerator.generateRandomString(3));

        transaction = new Transaction();
        transaction.setStatus(TransactionStatus.PENDING);
        transaction.setAmount(BigDecimal.valueOf(RandomGenerator.generateRandomInteger()));
        transaction.setType(TransactionType.DEPOSIT);
    }

    @AfterEach
    public void truncate() {
        user = null;
        account = null;
        transaction = null;
    }


    @Test
    @Rollback
    void findTransactionByUserIdTest() {
        userRepository.save(user);
        account.setUser(user);
        accountRepository.save(account);
        transaction.setAccount(account);
        transactionRepository.save(transaction);
        assertNotNull(transactionRepository.findTransactionsByUserId(user.getId()));
    }
}
