package com.egs.banktransaction.repository;

import com.egs.banktransaction.entity.Account;
import com.egs.banktransaction.entity.User;
import com.egs.banktransaction.utils.RandomGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TestAccountRepository {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    private User user;

    private Account account;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setUuid(RandomGenerator.generateUUID());
        user.setUsername(RandomGenerator.generateRandomString(6));
        user.setPassword( RandomGenerator.generateRandomString(16));
        user.setEmail( RandomGenerator.generateRandomString(4).concat("@gmail.com"));

        account = new Account();
        account.setBalance(BigDecimal.valueOf(RandomGenerator.generateRandomInteger()));
        account.setCurrency(RandomGenerator.generateRandomString(3));
    }

    @AfterEach
    public void truncate() {
        user = null;
        account = null;
    }

    @Test
    @Rollback
    void findAccountByUserTest() {
        userRepository.save(user);
        account.setUser(user);
        accountRepository.save(account);
        assertNotNull(accountRepository.findByUserId(account.getUser().getId()));
    }
}
