package com.egs.banktransaction.repository;


import com.egs.banktransaction.entity.User;
import com.egs.banktransaction.utils.RandomGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class TestUserRepository {

    @Autowired
    private UserRepository userRepository;

    private User user;

    @BeforeEach
    public void setUp() {
        String uuid = RandomGenerator.generateUUID();
        String username = RandomGenerator.generateRandomString(6);
        String password = RandomGenerator.generateRandomString(16);
        String email = RandomGenerator.generateRandomString(5).concat("@gmail.com");
        user = new User();
        user.setUuid(uuid);
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
    }

    @AfterEach
    public void truncate() {
        user = null;
    }

    @Test
    @Rollback
    void findByEmailTest() {
        userRepository.save(user);
        User fetchedUser = userRepository.findByEmail(user.getEmail());
        assertEquals(user.getUuid(), fetchedUser.getUuid());
        assertEquals(user.getUsername(), fetchedUser.getUsername());
        assertEquals(user.getEmail(), fetchedUser.getEmail());
        assertEquals(user.getPassword(), fetchedUser.getPassword());
    }

    @Test
    @Rollback
    void findByIdAndUuidTest() {
        userRepository.save(user);
        User fetchedUser = userRepository.findByIdAndUuid(user.getId(), user.getUuid());
        assertEquals(user.getId(), fetchedUser.getId());
        assertEquals(user.getUuid(), fetchedUser.getUuid());
        assertEquals(user.getUsername(), fetchedUser.getUsername());
        assertEquals(user.getEmail(), fetchedUser.getEmail());
        assertEquals(user.getPassword(), fetchedUser.getPassword());
    }
}
