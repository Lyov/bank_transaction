package com.egs.banktransaction.utils;

import java.util.Random;
import java.util.UUID;

public class RandomGenerator {

    public static Integer generateRandomInteger() {
        return new Random().nextInt();
    }

    public static Long generateRandomLong() {
        return new Random().nextLong();
    }

    public static String generateRandomString(int len) {
        String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
                + "lmnopqrstuvwxyz!@#$%&";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }

    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        String uuidAsString = uuid.toString();
        return uuidAsString;
    }
}
