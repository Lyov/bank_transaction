package com.egs.banktransaction.controller;

import com.egs.banktransaction.common.enumeration.Role;
import com.egs.banktransaction.common.enumeration.TransactionStatus;
import com.egs.banktransaction.common.enumeration.TransactionType;
import com.egs.banktransaction.entity.Account;
import com.egs.banktransaction.entity.Transaction;
import com.egs.banktransaction.entity.User;
import com.egs.banktransaction.entity.UserKey;
import com.egs.banktransaction.service.TransactionService;
import com.egs.banktransaction.service.UserService;
import com.egs.banktransaction.utils.Converter;
import com.egs.banktransaction.utils.RandomGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class TestUserController {

    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;

    @Mock
    TransactionService transactionService;

    @Autowired
    private MockMvc mockMvc;


    private User user = null;

    private List<Transaction> transactions;

    private Long id;

    private Date date;


    @BeforeEach
    void setup() {
        id = RandomGenerator.generateRandomLong();
        date = new Date();
        transactions = new ArrayList<>();

        User user = new User();
        user.setId(RandomGenerator.generateRandomLong());
        user.setUuid(RandomGenerator.generateUUID());
        user.setUsername(RandomGenerator.generateRandomString(6));
        user.setPassword( RandomGenerator.generateRandomString(16));
        user.setEmail( RandomGenerator.generateRandomString(6));

        UserKey userKey = new UserKey();
        userKey.setId(RandomGenerator.generateRandomLong());
        userKey.setRole(Role.ROLE_ADMIN);
        userKey.setTokenType(RandomGenerator.generateRandomString(5));
        userKey.setToken(RandomGenerator.generateRandomString(50));
        userKey.setRefreshToken(RandomGenerator.generateRandomString(50));
        userKey.setUserId(RandomGenerator.generateRandomLong());

        Account account = new Account();
        account.setId(RandomGenerator.generateRandomLong());
        account.setBalance(BigDecimal.valueOf(RandomGenerator.generateRandomLong()));
        account.setCurrency(RandomGenerator.generateRandomString(3));
        account.setUser(user);

        Transaction transaction = new Transaction();
        transaction.setId(RandomGenerator.generateRandomLong());
        transaction.setStatus(TransactionStatus.PENDING);
        transaction.setAmount(BigDecimal.valueOf(RandomGenerator.generateRandomLong()));
        transaction.setType(TransactionType.DEPOSIT);
        transaction.setAccount(account);
        transactions.add(transaction);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @AfterEach
    void tearDown() {
        user = null;
    }


    @Test
    void getUserByIdTest() throws Exception {
        Mockito.when(transactionService.getUserTransactions(any())).thenReturn(transactions);
        StringBuilder path = new StringBuilder("/api/users/");
        path.append(id);
        path.append("/transactions");
        mockMvc.perform(get(path.toString()).
                contentType(MediaType.APPLICATION_JSON).
                content(Converter.asJsonString(user))).
                andExpect(status().isOk()).
                andDo(MockMvcResultHandlers.print());
    }


}
